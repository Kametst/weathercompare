package Utilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

    public Properties propertyReader() throws IOException {


        //%s takes the alphabets before env which we pass in the program.
        //"user.dir" is used to get the users directory and after src it is going to be same for all

        FileReader fileReader = new FileReader(System

                .getProperty("user.dir")+"/src/main/resources/Data.properties");
        Properties properties = new Properties();
        properties.load(fileReader);
        return properties;

    }
}




/*

public class PropertyReader {}
*/

