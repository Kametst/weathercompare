package Client.OpenWeatherMapAPI;

import Utilities.PropertyReader;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class GetWeatherFromOpenMap {
    public double getWeatherFromOpenMap() throws IOException, ParseException {
        Properties properties = new PropertyReader().propertyReader();
        Response response = given()
                .queryParam("q",properties.getProperty("City"))
                .queryParam("appid",properties.getProperty("apiKey"))
                .queryParam("units", properties.getProperty("temp"))
                .when().get(properties.getProperty("ApiUrl"));

        JSONParser p = new JSONParser();
        Object obj = p.parse(response.asString());
        JSONObject jObj = (JSONObject) obj;
        JSONObject main = (JSONObject) jObj.get("main");

        //System.out.println(main.get("temp"));
        return (double) main.get("temp");
    }
}
