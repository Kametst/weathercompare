package Client.AccWeatherUI;

import BrowserSetup.ChooseBrowser;
import Utilities.PropertyReader;
import org.openqa.selenium.WebDriver;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class GetUiTemp {
    private static WebDriver driver;
    private Properties properties;

    public double getUiTemp() throws IOException {
        PropertyReader pr = new PropertyReader();
        properties = pr.propertyReader();
        ChooseBrowser chooseBrowser = new ChooseBrowser();
        driver= chooseBrowser.Browser();
        driver.get(properties.getProperty("Uiurl"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        SearchLocation searchLocation = new SearchLocation(driver);
        searchLocation.SearchLoc(properties.getProperty("City"));
        searchLocation.MoreDetails();
        searchLocation.RemAds();
        double UiTemp= Integer.parseInt(searchLocation.TempScreen().replaceAll("[^\\d]",""));

        driver.quit();
        return UiTemp;
    }
}
