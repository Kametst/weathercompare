package Client.AccWeatherUI;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchLocation extends PageInit{
    WebDriver driver;
    public SearchLocation(WebDriver driver) {
        super(driver);
        this.driver=driver;
    }

    @FindBy(xpath = "//input[@type='text' and @class='search-input']")
    WebElement Search;

    @FindBy(xpath = "/html/body/div/div[5]/div[1]/div[1]/a[1]")
    WebElement ClickMoreDetails;

    @FindBy(xpath = "//div[@id='dismiss-button']/div")
    WebElement ads;

    @FindBy(xpath = "//div[@class='display-temp']")
    WebElement temp;

    public void SearchLoc(String city){
        Search.sendKeys(city+Keys.RETURN);
    }
    public void MoreDetails(){
        ClickMoreDetails.click();
    }
    public void RemAds(){
        driver.switchTo().frame("google_ads_iframe_/6581/web/in/interstitial/weather/local_home_0");
        ads.click();
    }
    public String TempScreen(){
       return temp.getText();
    }

}
