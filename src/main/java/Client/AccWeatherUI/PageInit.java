package Client.AccWeatherUI;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageInit {
    public PageInit(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}