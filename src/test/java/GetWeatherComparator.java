
import BrowserSetup.ChooseBrowser;
import Client.AccWeatherUI.GetUiTemp;
import Client.AccWeatherUI.SearchLocation;
import Client.OpenWeatherMapAPI.GetWeatherFromOpenMap;
import Utilities.PropertyReader;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class GetWeatherComparator {

    @Test
    public void getLocationWeatherApi() throws ParseException, IOException {
        GetUiTemp UiTemp = new GetUiTemp();
        double AccuWeatherTemp = UiTemp.getUiTemp();
        System.out.println("\n \nTemperature Report from Accuweather: "+AccuWeatherTemp);


        GetWeatherFromOpenMap obj = new GetWeatherFromOpenMap();
        double OpenMapTemp = obj.getWeatherFromOpenMap();
        System.out.println("Temperature Report from OpenWeatherMap : "+OpenMapTemp);


        int Diff = (int)AccuWeatherTemp - (int)OpenMapTemp;
        System.out.println("\n Difference Between two Reports : "+Diff);

        if(Diff<=3 && Diff>-3){
            System.out.println("Test Passed");
        }
        else {
            System.out.println("Test Failed");
        }
    }

}
